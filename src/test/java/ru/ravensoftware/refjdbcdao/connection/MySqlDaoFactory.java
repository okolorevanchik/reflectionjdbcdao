package ru.ravensoftware.refjdbcdao.connection;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MySqlDaoFactory implements DaoFactory {

    private String login;
    private String password;
    private String url;

    private static DaoFactory mySqlDaoFactory;

    private MySqlDaoFactory() {
        try {
            FileInputStream inputStream = new FileInputStream("src/test/resources/db.property");
            Properties properties = new Properties();
            properties.load(inputStream);
            this.login = properties.getProperty("db.login");
            this.password = properties.getProperty("db.password");
            this.url = properties.getProperty("db.url");
            String driver = properties.getProperty("db.driver");
            Class.forName(driver);
        } catch (FileNotFoundException e) {
            System.err.println("File db.property in the path src/test/resources/ not found.");
        } catch (IOException e) {
            System.err.println("Error reading file db.property");
        } catch (ClassNotFoundException e) {
            System.err.println("Driver not found.");
        }
    }

    public static DaoFactory getInstance() {
        if (mySqlDaoFactory == null)
            mySqlDaoFactory = new MySqlDaoFactory();
        return mySqlDaoFactory;
    }

    @Override
    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, login, password);
    }
}
