package ru.ravensoftware.refjdbcdao.connection;

import java.sql.Connection;
import java.sql.SQLException;

public interface DaoFactory {
    public Connection getConnection() throws SQLException;
}
