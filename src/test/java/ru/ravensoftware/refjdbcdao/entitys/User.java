package ru.ravensoftware.refjdbcdao.entitys;

import ru.ravensoftware.refjdbcdao.annotations.Column;
import ru.ravensoftware.refjdbcdao.annotations.Entity;
import ru.ravensoftware.refjdbcdao.annotations.Key;
import ru.ravensoftware.refjdbcdao.annotations.Table;

@Entity
@Table(table = "users")
public class User {

    @Key
    @Column
    private long userId;
    @Column
    private String firstName;
    @Column
    @Key
    private String lastName;
    @Column
    private int age;
    @Column(name = "admin")
    private boolean isAdmin;

    public User() {
    }

    public User(String firstName, String lastName, int age, boolean isAdmin) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.isAdmin = isAdmin;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (age != user.age) return false;
        if (isAdmin != user.isAdmin) return false;
        if (userId != user.userId) return false;
        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
        if (!lastName.equals(user.lastName)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (userId ^ (userId >>> 32));
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + lastName.hashCode();
        result = 31 * result + age;
        result = 31 * result + (isAdmin ? 1 : 0);
        return result;
    }
}
