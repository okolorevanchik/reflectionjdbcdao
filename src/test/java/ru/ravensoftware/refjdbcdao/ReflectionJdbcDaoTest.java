package ru.ravensoftware.refjdbcdao;


import org.junit.*;
import ru.ravensoftware.refjdbcdao.connection.DaoFactory;
import ru.ravensoftware.refjdbcdao.connection.MySqlDaoFactory;
import ru.ravensoftware.refjdbcdao.entitys.User;
import ru.ravensoftware.refjdbcdao.impl.ReflectionJdbcDaoImpl;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ReflectionJdbcDaoTest extends Assert {

    private static final DaoFactory DAO_FACTORY = MySqlDaoFactory.getInstance();

    private Connection connection;

    @BeforeClass
    public static void createTable() {
        executeScript("./src/test/resources/generateTestTable.sql");
    }

    @AfterClass
    public static void deleteTable() {
        executeScript("./src/test/resources/deleteTestTable.sql");
    }

    private static void executeScript(String path) {
        String query = getQueryInFile(path);
        try (
                Connection connection = DAO_FACTORY.getConnection();
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static String getQueryInFile(String path) {
        StringBuilder sb = new StringBuilder();
        try (
                BufferedReader in = new BufferedReader(new FileReader(path))
        ){
            String string;
            while ((string = in.readLine()) != null) {
                sb.append(string).append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    @Before
    public void setUp() throws Exception {
        connection = DAO_FACTORY.getConnection();
    }

    @After
    public void tearDown() throws Exception {
        String query = getQueryInFile("./src/test/resources/clearTestTable.sql");
        try (
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        connection.close();
    }

    @Test
    public void testInsert() throws Exception {
        ReflectionJdbcDao<User> dao = new ReflectionJdbcDaoImpl<>(connection, User.class);

        User user = new User("Dmitriy", "Tolchinskiy", 24, true);
        user.setUserId(1);

        dao.insert(user);

        User userTest = dao.selectByKey(user);
        assertEquals("test insert method", userTest, user);
    }

    @Test
    public void testUpdate() throws Exception {
        ReflectionJdbcDao<User> dao = new ReflectionJdbcDaoImpl<>(connection, User.class);

        User firstUser = new User(null, "Grashin", 0, false);
        firstUser.setUserId(2);
        dao.insert(firstUser);
        User user = new User("Artem", "Grashin", 25, true);
        user.setUserId(2);

        dao.update(user);

        User userTest = dao.selectByKey(user);
        assertEquals("test update method", userTest, user);
    }

    @Test
    public void testDeleteByKey() throws Exception {
        ReflectionJdbcDao<User> dao = new ReflectionJdbcDaoImpl<>(connection, User.class);

        User user = new User("Dmitriy", "Tolchinskiy", 24, true);
        user.setUserId(3);
        dao.insert(user);

        dao.deleteByKey(user);

        User userTest = dao.selectByKey(user);
        assertEquals("test delete method", userTest, null);

    }

    @Test
    public void testSelectByKey() throws Exception {
        ReflectionJdbcDao<User> dao = new ReflectionJdbcDaoImpl<>(connection, User.class);

        User firstUser = new User(null, "Grashin", 0, false);
        firstUser.setUserId(4);
        dao.insert(firstUser);


        User userTest = dao.selectByKey(firstUser);
        assertEquals("test selectByKey method", userTest, firstUser);
    }

    @Test
    public void testSelectAll() throws Exception {
        ReflectionJdbcDao<User> dao = new ReflectionJdbcDaoImpl<>(connection, User.class);
        List<User> list = new ArrayList<>();

        User user1 = new User("Dmitriy", "Tolchinskiy", 24, true);
        user1.setUserId(1);
        User user2 = new User("Artem", "Grashin", 25, true);
        user2.setUserId(2);
        User user3 = new User(null, "Grashin", 0, false);
        user3.setUserId(3);

        list.add(user1);
        dao.insert(user1);
        list.add(user2);
        dao.insert(user2);
        list.add(user3);
        dao.insert(user3);

        List<User> result = dao.selectAll();
        for (int i = 0; i < result.size(); i++) {
            assertEquals("test " + i + " selectAll method", result.get(i), list.get(i));
        }
    }
}
