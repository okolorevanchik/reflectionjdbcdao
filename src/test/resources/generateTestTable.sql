CREATE TABLE users (
    user_id INT NOT NULL,
    first_name VARCHAR(50),
    last_name VARCHAR(50) NOT NULL,
    age INT NOT NULL,
    admin BIT NOT NULL,
    PRIMARY KEY (user_id, last_name)
);