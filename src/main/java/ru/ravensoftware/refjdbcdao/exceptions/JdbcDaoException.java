package ru.ravensoftware.refjdbcdao.exceptions;

public class JdbcDaoException extends RuntimeException {
    public JdbcDaoException(final String message) {
        super(message);
    }
}
