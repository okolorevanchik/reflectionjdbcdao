package ru.ravensoftware.refjdbcdao.impl;

import ru.ravensoftware.refjdbcdao.ReflectionJdbcDao;
import ru.ravensoftware.refjdbcdao.annotations.Column;
import ru.ravensoftware.refjdbcdao.annotations.Entity;
import ru.ravensoftware.refjdbcdao.annotations.Key;
import ru.ravensoftware.refjdbcdao.annotations.Table;
import ru.ravensoftware.refjdbcdao.exceptions.JdbcDaoException;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReflectionJdbcDaoImpl<T> implements ReflectionJdbcDao<T> {

    private Connection connection;
    private Class<T> tClass;
    private String tableName;

    public ReflectionJdbcDaoImpl(Connection connection, Class<T> tClass) {
        Entity entity = tClass.getAnnotation(Entity.class);
        if (entity == null)
            throw new JdbcDaoException("There is no annotations @Entity.");
        Table table = tClass.getAnnotation(Table.class);
        if (table == null)
            throw new JdbcDaoException("There is no annotations @Table.");

        this.connection = connection;
        this.tClass = tClass;
        this.tableName = table.table();
    }

    @Override
    public void insert(T object) {
        if (object == null)
            throw new JdbcDaoException("Parameter of a method \"insert\" is null.");

        String query = getInsertQuery(tableName,
                getColumnsNameAndValues(object, Column.class)
        );

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();
        } catch (SQLException e) {
            throw new JdbcDaoException("Error in query: " + query);
        }
    }

    @Override
    public void update(T object) {
        if (object == null)
            throw new JdbcDaoException("Parameter of a method \"update\" is null.");

        String query = getUpdateQuery(tableName,
                getColumnsNameAndValues(object, Column.class),
                getColumnsNameAndValues(object, Key.class)
        );

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            int  count = statement.executeUpdate();
            if (count != 1)
                throw new JdbcDaoException("On update modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new JdbcDaoException("Error in query: " + query);
        }

    }

    @Override
    public void deleteByKey(T key) {
        if (key == null)
            throw new JdbcDaoException("Parameter of a method \"deleteByKey\" is null.");

        String query = getDeleteQuery(tableName,
                getColumnsNameAndValues(key, Key.class));

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            int count = statement.executeUpdate();
            if (count != 1)
                throw new JdbcDaoException("On delete modify more then 1 record: " + count);
        } catch (SQLException e) {
            throw new JdbcDaoException("Error in query: " + query);
        }
    }

    @Override
    public T selectByKey(T key) {
        if (key == null)
            throw new JdbcDaoException("Parameter of a method \"selectByKey\" is null.");

        List<T> list;
        String query = getSelectQuery(tableName,
                getColumnsNameAndValues(key, Key.class));

        try (
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            ResultSet resultSet = statement.executeQuery();
            list = parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new JdbcDaoException("Error in query: " + query);
        }

        if (list == null || list.size() == 0)
            return null;
        if (list.size() > 1)
            throw new JdbcDaoException("Received more than one record.");

        return list.iterator().next();
    }

    @Override
    public List<T> selectAll() {
        String query = getAllQuery(tableName);
        try (
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            ResultSet resultSet = statement.executeQuery();
            return parseResultSet(resultSet);
        } catch (SQLException e) {
            throw new JdbcDaoException("Error in query: " + query);
        }
    }

    private List<T> parseResultSet(ResultSet resultSet) {
        if (resultSet == null)
            return null;

        List<T> list = new ArrayList<>();
        try {
            while (resultSet.next()) {
                T object = tClass.newInstance();
                Method[] methods = object.getClass().getMethods();
                for (Method  method: methods) {
                    String methodName = method.getName();
                    if (!methodName.startsWith("set"))
                        continue;
                    Object rs = resultSet.getObject(getTableStyleForColumn(methodName));
                    method.invoke(object, rs);
                }
                list.add(object);
            }
            return list;

        } catch (SQLException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new JdbcDaoException("In the JavaBean class not getters/setters methods.");
        }
    }

    private Map<String, String> getColumnsNameAndValues(T object, Class annotation) {
        Field[] fields = tClass.getDeclaredFields();
        Map<String, String> map = new LinkedHashMap<>();

        if (fields == null || fields.length == 0)
            throw new JdbcDaoException("There are no private fields in JavaBean a class.");

        try {
            for (Field field : fields) {
                field.setAccessible(true);
                if (field.getAnnotation(annotation) != null) {
                    String keyMap;
                    if (field.getAnnotation(Column.class).name().isEmpty())
                        keyMap = getTableStyleForColumn(field.getName());
                    else keyMap = field.getAnnotation(Column.class).name();

                    Object valueObject = field.get(object);
                    if (valueObject != null) {
                        String value = valueObject.toString();
                        if (field.getType() == boolean.class || field.getType() == Boolean.class) {
                            if (value.equals("true")) value = "1";
                            else value = "0";
                        } else value = "'" + value + "'";
                        map.put(keyMap, value);
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if (map.isEmpty())
            throw new JdbcDaoException("There is no annotations @Column or @Key.");

        return map;
    }

    private String getTableStyleForColumn(String name) {
        String[] arrString = name.split("[A-Z]");
        Pattern p = Pattern.compile("[A-Z]+");
        Matcher m = p.matcher(name);
        StringBuilder builder = new StringBuilder(arrString[0]);
        for (int i = 1; m.find(); i++) {
            builder.append("_").append(m.group().substring(0, 1).toLowerCase()).append(arrString[i]);
        }

        String result = builder.toString();
        if (result.startsWith("set")) {
            builder.delete(0,4);
            result = builder.toString();
        }

        return result;
    }

    private String getAllQuery(final String tableName) {
        return "SELECT * FROM " + tableName + ";";
    }

    private String getSelectQuery(final String name, final Map<String, String> map) {
        StringBuilder result = new StringBuilder("SELECT * FROM ")
                .append(name).append(" WHERE");
        addSubstringNew(result, map);
        return result.append(";").toString();
    }

    private String getInsertQuery(final String name, final Map<String, String> stringMap) {
        StringBuilder beginQuery = new StringBuilder("INSERT INTO ")
                .append(name).append(" (");
        StringBuilder endQuery = new StringBuilder("VALUES (");
        for (Map.Entry<String, String> map: stringMap.entrySet()) {
            beginQuery.append(map.getKey()).append(", ");
            endQuery.append(map.getValue()).append(", ");
        }
        beginQuery.delete(beginQuery.length() - 2, beginQuery.length())
                .append(") ");
        endQuery.delete(endQuery.length() - 2, endQuery.length())
                .append(");");
        return beginQuery.toString() + endQuery.toString();
    }

    private String getUpdateQuery(final String name, final Map<String, String> column, final Map<String, String> key) {
        StringBuilder result = new StringBuilder("UPDATE ")
                .append(name).append(" SET");
        addSubstring(result, column);
        result.append(" WHERE");
        addSubstringNew(result, key);
        return result.append(";").toString();
    }

    private String getDeleteQuery(final String name, final Map<String, String> map) {
        StringBuilder sb = new StringBuilder("DELETE FROM ")
                .append(name).append(" WHERE");
        addSubstringNew(sb, map);
        return sb.append(";").toString();
    }

    private void addSubstring(final StringBuilder str, final Map<String, String> mapString) {
        for (Map.Entry<String, String> map: mapString.entrySet()) {
            str.append(" ").append(map.getKey()).append("= ").append(map.getValue()).append(",");
        }
        str.delete(str.length() - 1, str.length());
    }

    private void addSubstringNew(final StringBuilder str, final Map<String, String> mapString) {
        for (Map.Entry<String, String> map: mapString.entrySet()) {
            str.append(" ").append(map.getKey()).append("= ").append(map.getValue()).append(" AND");
        }
        final int constant = 4;
        str.delete(str.length() - constant, str.length());
    }
}
