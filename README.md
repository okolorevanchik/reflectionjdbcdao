## Test task in Yandex. ##

DAO (Data Access Object) service, which is using
Reflection does display a certain class format 
(by type JavaBean) in a table in the database.

The table name and key fields are configured via
annotations. The key can be constructed.

Name column in the table corresponding to the field in the class,
given either through annotation, or replacement of "lower camel
case" in the title field of the class (userId) to underscore (user_id).